(function($) {
  $('.js-home-slider, .js-post-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });
  $('.js-training-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });
  $('.js-product-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });

  //slide menu on click
  $('button.btn-menu-mobile').click(function(){
    $('body').toggleClass('slide');
    $('header').toggleClass('slide');
    $('#dl-menu').toggleClass('slide');
    $('button.dl-trigger').trigger("click");
    return false;
  });
  $(window).click(function() {
    $('body').removeClass('slide');
    $('header').removeClass('slide');
    $('#dl-menu').removeClass('slide');
  });

  //menu mobile
  $( '#dl-menu' ).dlmenu({
    animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
  });


  // maps info
  var locationInfo = $('.js-location-info');
  var openLocationInfo = $('.js-open-location-info');

  openLocationInfo.click(function() {
    locationInfo.toggleClass('infoOpen');
  });

  // maps filter
  var filterWrapper = $('.js-filter-wrapper');
  var toggleFilter = $('.js-toggle-filter');
  var filterOption = $('.js-filter-option');
  var moreOptionWrapper = $('.js-more-option-wrapper');
  var moreOption = $('.js-more-option');
  var showMore = $('.js-show-more');

  toggleFilter.click(function() {
    filterWrapper.toggleClass('filterOpen');
    if (filterWrapper.hasClass('filterOpen') == false) {
      filterOption.slideUp();
    } else {
      filterOption.slideDown();
    }
  });

  showMore.click(function() {
    moreOptionWrapper.toggleClass('moreOpen');
    if(moreOptionWrapper.hasClass('moreOpen') == false) {
      moreOption.slideUp();
      $(this).find('a').html('Show more <span></span>');
    } else {
      moreOption.slideDown();
      $(this).find('a').html('Hide <span></span>');
    }
  });

  // $('.btn-menu-mobile').click(function(){
  //   $('body').toggleClass('slide');
  //   $('#mobileMenu').toggleClass('slide');

  //   // var x = $('#mobileMenu');
  //   // if($('body').attr('class') === "front slide") {
  //   //   $(document).not(x).click(function(){
  //   //   console.log(x);
  //   //       $('body').removeClass('slide');
  //   //       $('#mobileMenu').removeClass('slide');
  //   //   });
  //   // }
  //  return false;
  // });

  //padding top body
  // $(window).load(function(){
  // var top = $('header').height();
  //   $('body').css('padding-top', top);
  //   $(window).resize(function(){
  //     $('body').css('padding-top', top);
  //   });
  // });

  // Filter Product Checkbox
  // $("[data-attr=all]").click(function(){
  //  var className = $(this).attr("class");
  //  if( $(this).is(":checked") == true ){
  //    $("." + className).prop("checked", true);
  //    $("." + className).parent().addClass("active");
  //  }
  //  else {
  //    $("." + className).prop("checked", false);
  //    $("." + className).parent().removeClass("active");
  //  }
  // });
  $("[data-attr=all]").click(function(){
      if( $(this).is(":checked") == true ){
          $(this).parent().parent().find('.facet-item ').addClass('active');
          $(this).parent().parent().find('.facet-item ').find('input').prop("checked", true);
      }
      else {
          $(this).parent().parent().find('.facet-item ').removeClass('active');
          $(this).parent().parent().find('.facet-item ').find('input').prop("checked", false);
      }
  });
  $(".filter .facet-item label").click(function(){
      var className = $(this).parent().parent();
      $(this).parent('.facet-item').toggleClass("active");
      $(this).parent('.facet-item ').find('input').trigger('click');
      //$(this).parent('.facet-item ').find('ul').toggle('show');
  });
  $(".filter .facet-item input").change(function(){
    if( $(this).is(":checked") == true ){
      $(this).parent().addClass("active");
    }
    else {
      $(this).parent().removeClass("active");
    }
  });

  $(window).scroll(function( e ){
    var winHeight = $(document).height();
    var bodyScroll = $('body').scrollTop();


    //padding top body
    // $(window).load(function(){
    // var top = $('header').height();
    //   $('body').css('padding-top', top);
    //   $(window).resize(function(){
    //     $('body').css('padding-top', top);
    //   });
    // });

    // Filter Product Checkbox
    // $("[data-attr=all]").click(function(){
    // 	var className = $(this).attr("class");
    // 	if( $(this).is(":checked") == true ){
    // 		$("." + className).prop("checked", true);
    // 		$("." + className).parent().addClass("active");
    // 	}
    // 	else {
    // 		$("." + className).prop("checked", false);
    // 		$("." + className).parent().removeClass("active");
    // 	}
    // });
    $("[data-attr=all]").click(function(){
        if( $(this).is(":checked") == true ){
            $(this).parent().parent().find('.facet-item ').addClass('active');
            $(this).parent().parent().find('.facet-item ').find('input').prop("checked", true);
        }
        else {
            $(this).parent().parent().find('.facet-item ').removeClass('active');
            $(this).parent().parent().find('.facet-item ').find('input').prop("checked", false);
        }
    });
    $(".filter .facet-item label").click(function(){
       var className = $(this).parent().parent()
        $(this).parent('.facet-item').toggleClass("active");
        $(this).parent('.facet-item ').find('input').trigger('click');
    });
    $(".filter .facet-item input").change(function(){
    	if( $(this).is(":checked") == true ){
    		$(this).parent().addClass("active");
    	}
    	else {
    		$(this).parent().removeClass("active");
    	}
    });

    if($(window).scrollTop() + $(window).height() == $(document).height()) {
      $(".right-contact").animate({
        "marginTop": "auto"
      }, "slow" );
    } else {
      $(".right-contact").stop().animate({
        "marginTop": ($(window).scrollTop()) + "px"
      }, "slow" );
    }
    // var totaltext = "";
    // for (var i = 0; i < 100; i++) {
    //   totaltext += "scroll!<br />";
    // }
    //$("#div2").html(totaltext);

  });
  // var totaltext = "";
  // for (var i = 0; i < 100; i++) {
  //   totaltext += "scroll!<br />";
  // }
  //$("#div2").html(totaltext);
  topRightContact();
  feauredWidth();
  slideFeatured();
  resizeImgFlex();
  $(window).load(function(){
    topRightContact();
    feauredWidth();
    slideFeatured();
    resizeImgFlex();
  });
  $(window).resize(function(){
    topRightContact();
    feauredWidth();
    slideFeatured();
    resizeImgFlex();
    resizeSearchBox();
  });

  // scroll btn scroll
  $(window).scroll(function() {
    var dhdr;
    if($(window).width() >= 1440) {
      dhdr = $('.detail-header').outerHeight() + 252;
    } else {
      dhdr = $('.detail-header').outerHeight() + 183;
    }
    var dth = $('.detail-menu');
    var mns = "main-nav-scrolled";

    if ($(window).scrollTop() >= 150 ) {

      $('.landing-scroll').fadeOut();

    } else {
      $('.landing-scroll').fadeIn();
    }
  if($(window).width() >= 992) {
    if ( $(this).scrollTop() >= dhdr) {
      $('.prod-header').hide();
      dth.addClass(mns);
      //return;
      // var navMenu = $('.nav-menu');
    } else {
      $('.prod-header').show();
      dth.removeClass(mns);
      //return;
    }
  }
  });
  function topRightContact() {
    var winHeight = $(window).height();
    var rcHeight = $(".right-contact").height();
    var top = winHeight - rcHeight;
    $(".right-contact").css('top', top + 1);
  }

  function feauredWidth() {
    var winWidth = $(window).width();
    var fWidth = $(".right-contact").width();
    var width = winWidth - fWidth;
    $(".featured").css('width', width);
  }

  function slideFeatured() {
    if($(window).width() <= 992 && $(window).width() >= 768) {
      $('#featuredSlider').addClass('js-featured-slider');
      $('.js-featured-slider').owlCarousel({
        nav: false,
        items:3,
        loop:true,
        autoplay:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
          1024:{
            items:3,
            loop:false
          },
          767:{
            items:3,
          }
        }
      });
    }
    if($(window).width() <= 767) {
      // $('.js-featured-slider').data('owlCarousel').destroy();
    }
    if($(window).width() >= 1025) {
      $('#featuredSlider').removeClass('js-featured-slider');
    }
    return;
  }

  if($(window).width() < 767) {
    var initwhidth = $(".popup-call-center").width();
    $(".call-center a").click(function(){
      // $('.big-number').css({
      //   'width': '160px',
      //   'height': '36px',
      //   'position': 'fixed',
      //   'top': '50%',
      //   'left': '50%',
      //   'margin': '18px 0 0 -80px'
      // });
      $('.big-number').toggleClass('show');
    });
  }

  $("#specification + .panel-group .panel:first-child .panel-heading a, #feature + .panel-group .panel:first-child .panel-heading a").removeClass("collapsed");
  $("#specification + .panel-group .panel:first-child .panel-collapse, #feature + .panel-group .panel:first-child .panel-collapse").addClass("in");


    $("#btn-search-close").click(function(){

    // $(".search-box").animate({
    //   "opacity": "0",
    //   "z-index": "77",
    // },"700");
    //$(".nav-right").show();

    $(".search-box").fadeOut();
    $("#btn-search").css('z-index', '88');
  });

  $("#btn-search").click(function(){
    resizeSearchBox();
    var logoWidth = $('.logo').width();
    var space = 220;
    var widthBlank = logoWidth + space;
    var winWidth = $(window).width();
    var srcBoxWitdh = winWidth - widthBlank;
    $(".search-box").width(srcBoxWitdh);
    $(".search-box").fadeIn();
    // if ($(window).width() <= 767) {
    //   $(".search-box").animate({
    //     "opacity": "1",
    //     "display": "block",
    //     "z-index": "999",
    //   }, 100);
    // } else if ($(window).width() <= 1440) {
    //   $(".search-box").animate({
    //     "opacity": "1",
    //     "display": "block",
    //     "z-index": "98",
    //   }, 100);
    // }else {
    //   $(".search-box").animate({
    //     "opacity": "1",
    //     "display": "block",
    //     "z-index": "98",
    //   }, 100);
    // }
    //$(".nav-right").hide();
    $("#btn-search").css('z-index', '86');
  });


  // menu scrollspy on click
  $('.nav-menu li a').on('click', function(event) {
      var sctp;
      if($(window).width() >= 1440) {
        if ($(window).scrollTop() >= $('.detail-header').outerHeight() + 252) {
          sctp = 100;
        } else {
          sctp = 166;
        }
      } else {
        if ($(window).scrollTop() >= $('.detail-header').outerHeight() + 183) {
          sctp = 100;
        } else {
          sctp = 162;
        }
      }
      var $anchor = $(this);
      $('html, body').stop().animate({
          scrollTop: ($($anchor.attr('href')).offset().top - sctp)
      }, 1000, 'linear');
      $anchor.parent().parent().find('.active').removeClass('active');
      $anchor.parent().addClass('active');
      event.preventDefault();
  });
  //resize search box
  function resizeSearchBox() {
    var logoWidth = $('.logo').width();
    var space = 100;
    var widthBlank = logoWidth + space;
    var winWidth = $(window).width();
    var srcBoxWitdh = winWidth - widthBlank;
    if ($(window).width() <= 767) {
      $(".search-box").animate({
        "width": "100%",
        "z-index": "999"
      }, 100);
    } else if ($(window).width() <= 1440) {
      $(".search-box").animate({
        "width": srcBoxWitdh
      }, 100);
    }else {
      $(".search-box").animate({
        "width": srcBoxWitdh
      }, 100);
    }
  }

  //resize image flexslider history
  function resizeImgFlex(){
    var windowWidth = $(window).width();
    $('.shadow-bg img').width(windowWidth);
  }

  //quote style checkbox
  $('.checkbox.group-input > label').click(function(){
    var checkBox = $(this).find('input[type="checkbox"]');
    if( checkBox.is(":checked")){
      $(this).addClass('active');
    }
    else {
      $(this).removeClass('active');
    }
  });
   //quote style radio
  $('.radio.group-input > label').click(function(){
    var radioBox = $(this).find('input[type="radio"]');
    $(this).parent().find('.active').removeClass('active');
    if( radioBox.is(":checked")){
      $(this).addClass('active');
    }
  });
  //mobile language
  $('.js-menu-lang').html($('#menu-lang').html());
  $('#menu-lang').remove();

  // CV Upload
  $("label.js-label-cv-upload").on('click',function () {
    $(".js-cv-upload input").trigger('click');
  });

  $(".js-cv-upload input").on('change',function(){
    var file = $('.js-cv-upload input[name="field_cv[0][fids]"]').val();
    if(file == '') {
      $('.js-label-cv-upload').html('Max 2MB and PDF format are supported.<span><img src="/themes/custom/trakindo_2016/images/upload-icon.png" alt="trakindo"></span>');
    }
  });

  $('#contact-message-career-opportunities-form').on('submit', function(){
    var file = $('.js-cv-upload input[name="field_cv[0][fids]"]').val();
    if(file == '') {
      if($('.js-cv-upload [aria-label="Error message"] ul').length > 0){
        if($('.js-cv-upload [aria-label="Error message"] ul li.error-file').length == 0) {
          $('.js-cv-upload [aria-label="Error message"] div > ul').append('<li class="error-file">This Field is required</li>');
        }
      } else {
        $('.js-cv-upload').append('<div aria-label="Error message"><div><ul><li class="error-file">This Field is required</li></ul></div></div>');
      }
      return false;
    }
  });

  $('.js-newsletter-form').on('submit',function(){
    $('#newsletter-modal').modal('show');
    $('.js-input-email input').val($('.js-newsletter-form .newsletter-email').val());
  });

  var lastScrollTop = 0;
  var padding = $("body").css("padding-top");

  if($(window).height() < 480){
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       if (st > lastScrollTop){
           // downscroll code
           $("header").hide();
           $(".right-contact").show();
           $("body").css("padding-top",0);
       } else {
          // upscroll code
          $("header").show();
          $("body").css("padding-top",padding);
          $(".right-contact").hide();
       }
       lastScrollTop = st;
    });
  }

  $('.js-promo-badge').click(function () {
    $(this).parent().closest('.product-item').find('.js-promotion-popup').toggleClass('show-down');
  });
  $('.js-promotion-popup .close-link').click(function () {
    $(this).parent().closest('.js-promotion-popup').toggleClass('show-down');
  });
  $(document).ready(function(){
    //var visit = $.cookie("firstvisit");
    var visit;
    if (visit==null){
      setTimeout(function() {
        $('#firstpopup').popup({autoopen: true, transition: 'all 0.3s'});
      }, 1000);
      $.cookie("firstvisit", "1", {expires: 7});
    }
    $("a.recenter").click(function(){
      var i = Drupal.geolocation.maps.length - 1;

      var latlong = $(this).attr("data-latlng").split(",");
      Drupal.geolocation.maps[i].googleMap.setCenter({lat: eval(latlong[0]), lng: eval(latlong[1])});
    });
    $('.checkbox.group-input input[type="checkbox"]').filter(':checked').closest('label').addClass('active');
  });

  //compar close item
  $('.compar-remove-item').click(function(){
    $(this).parent().parent().remove();
    return false;
  });

  $('.newsletter-title').blink({delay: 1000});

  //Product detail image 360
  $('.btn-360').click(function(){
    var link = $(this).parent().find('.exterior-360').text();
    var link2 = $(this).parent().find('.interior-360').text();
    var iframe = '<iframe src="'+link+'" width="620" height="356"></iframe>';
    if (link2) {
      var iframe = '<ul class="tab-link list-inline"><li class="active"><a role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" href="#tab-one">360° Exterior View</a></li><li><a role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" href="#tab-two">360° Interior View</a></li></ul><div class="tab-content"><div class="tab-pane active" id="tab-one"><iframe src="'+link+'" width="620" height="356"></iframe></div><div class="tab-pane" id="tab-two"><iframe src="'+link2+'" width="620" height="356"></iframe></div></div>';
    }
    $('#modal-360 .modal-body').html(iframe);
  });

  //Sort By Name for Product
  $('.js-sort-item-name a.sort-asc').click(function(){
    $('.js-select-sortby select[name="sort_bef_combine"]').val('name ASC').trigger('change');
    $('.sortby-name a.dropdown').html('Name Asc <i class="fa fa-angle-down"></i>');
  });
  $('.js-sort-item-name a.sort-desc').click(function(){
    $('.js-select-sortby select[name="sort_bef_combine"]').val('name DESC').trigger('change');
    $('.sortby-name a.dropdown').html('Name Desc <i class="fa fa-angle-down"></i>');
  });

  //Sort By Title for promotion
  $('.js-sort-item-title a.sort-asc').click(function(){
    $('.js-select-sortby select[name="sort_bef_combine"]').val('title ASC').trigger('change');
    $('.sortby-name a.dropdown').html('Name Asc <i class="fa fa-angle-down"></i>');
  });
  $('.js-sort-item-title a.sort-desc').click(function(){
    $('.js-select-sortby select[name="sort_bef_combine"]').val('title DESC').trigger('change');
    $('.sortby-name a.dropdown').html('Name Desc <i class="fa fa-angle-down"></i>');
  });

  //Call Center on mobile
  $('.right-contact-link').click(function(){
    if ($(window).width() <= 767) {
      $('#callcenter-modal').modal('show');
    }
  });
  $('.cancel-btn, .call-btn').click(function(){
    $('#callcenter-modal').modal('hide');
  });

  //Move compare table first child into each column
  $('.product-feature .tbl-spec tr').each(function(index, value) {
    if ($(window).width() <= 767) {
      $('td:eq(0)',  value).hide();
      var header = $('td:eq(0)',  value).text();
      $('td:gt(0)', value).prepend('<div class="tbl-head">' + header + '</div>');
    }
  });

})(jQuery);

(function ($, Drupal) {
  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {
      var compare1 = $('.js-compare-0 .compare-title').length;
      var compare2 = $('.js-compare-1 .compare-title').length;
      var compare3 = $('.js-compare-2 .compare-title').length;
      var compare4 = $('.js-compare-3 .compare-title').length;
      var compare = compare1+compare2+compare3+compare4;
      if(compare <= 1) {
        $('.compare-btn .btn-primary').attr("disabled","disabled");
      } else {
        $('.compare-btn .btn-primary').removeAttr("disabled");
      }

      if($('.js-cv-upload input[value="Remove"], .js-cv-upload input[value="Hapus"]', context).length == 1) {
        $('.js-label-cv-upload').addClass('has-remove');
        $('.js-cv-upload [aria-label="Error message"]').remove();
      }
      else {
        $('.js-label-cv-upload.has-remove').html('Max 2MB and PDF format are supported.<span><img src="/themes/custom/trakindo_2016/images/upload-icon.png" alt="trakindo"></span>');
        $('.js-label-cv-upload').removeClass('has-remove');
      }
    }
  };
})(jQuery, Drupal);